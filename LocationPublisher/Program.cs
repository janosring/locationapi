﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using LocationModel;

namespace LocationPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var location = new Location
            {
                Latitude = 55.86515m, 
                Longitude = -4.25763m 
            };

            while (true)
            {
                SendLocation(location);
                location.Longitude = location.Longitude + 0.01m;
                //location.Latitude =  location.Longitude - 0.001m;
                Console.WriteLine($"New Location: {location.Latitude}, {location.Longitude}");
                Thread.Sleep(5000);


            }
            
        }

        private static void SendLocation(Location baseLocation)
        {
            var client = new HttpClient();
            var url =
                $"http://localhost:55424/api/location?latitude={baseLocation.Latitude}&longitude={baseLocation.Longitude}";
            var task = client.PostAsync(url, null);

            Task.WaitAll(task);
        }
    }
}
