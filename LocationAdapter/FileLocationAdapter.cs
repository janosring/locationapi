﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using LocationModel;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LocationAdapter
{
    public class FileLocationAdapter : ILocationAdapter
    {
        private readonly string _fileLocation;

        public FileLocationAdapter(IConfiguration configuration)
        {
            _fileLocation = configuration["Config:LocationAdaptorFile"];
        }

        public Location GetLocation()
        {
            if (!File.Exists(_fileLocation)) return null;

            var locations = JsonConvert.DeserializeObject<List<Location>>(File.ReadAllText(_fileLocation));

            return locations.OrderByDescending(location => location.TimeStamp).FirstOrDefault();
        }

        public bool SaveLocation(Location location)
        {
            if (!File.Exists(_fileLocation)) return false;

            var locations = JsonConvert.DeserializeObject<List<Location>>(File.ReadAllText(_fileLocation));
            locations.Add(location);
            File.WriteAllText(_fileLocation, JsonConvert.SerializeObject(locations));

            return true;
        }
    }
}
