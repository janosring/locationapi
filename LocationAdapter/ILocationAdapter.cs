﻿using LocationModel;

namespace LocationAdapter
{
    public interface ILocationAdapter
    {
        Location GetLocation();
        bool SaveLocation(Location location);
    }
}