﻿using System;
using LocationAdapter;
using LocationModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace LocationAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class LocationController : Controller
    {
        private readonly ILocationAdapter _locationAdapter;

        public LocationController(ILocationAdapter locationAdapter)
        {
            _locationAdapter = locationAdapter;
        }

        // GET api/values
        [HttpGet]
        public Location Get()
        {
            return _locationAdapter.GetLocation();
        }

        // POST api/values
        [HttpPost]
        public bool Post([FromQuery]decimal latitude, [FromQuery] decimal longitude)
        {
            return _locationAdapter.SaveLocation(new Location
                {Latitude = latitude, Longitude = longitude, TimeStamp = DateTime.UtcNow});
        }
    }
}
